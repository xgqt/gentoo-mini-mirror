#!/bin/sh


# This file is part of gentoo-mini-mirror.

# gentoo-mini-mirror is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# gentoo-mini-mirror is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with gentoo-mini-mirror.  If not, see <https://www.gnu.org/licenses/>.


base="$(dirname "${0}")"


# Binpkgs

[ -f "${base}"/public/binpkgs/index.html ] && rm "${base}"/public/binpkgs/index.html

cat >> "${base}"/public/binpkgs/index.html << EOF
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="author" content="XGQT"/>
        <meta name="generator" content="GitLab Pages"/>
        <title>Binpkgs</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../assets/styles/main.css"/>
    </head>
    <body>
        <h1>
            Gentoo Mini Mirror - Binpkgs
        </h1>
EOF

for category in "${base}"/public/binpkgs/*
do
    cat >> "${base}"/public/binpkgs/index.html << EOF
        <b>$(basename "${category}")</b>
        <br />
EOF
    for package in "${category}"/*
    do
        echo "${package}"
        name="$(basename "${category}")/$(basename "${package}")"
        cat >> "${base}"/public/binpkgs/index.html << EOF
        <a href="${name}">
            ${name}
        </a>
        <br />
EOF
    done
done

cat >> "${base}"/public/binpkgs/index.html << EOF
        <br />
        <br />
        <a href="https://gitlab.com/xgqt/gentoo-mini-mirror/-/pipelines">
            <i>Generated on: $(date +"%Y-%m-%d")</i>
        </a>
        <br />
    </body>
</html>
EOF


# Distfiles

[ -f "${base}"/public/distfiles/index.html ] && rm "${base}"/public/distfiles/index.html

cat >> "${base}"/public/distfiles/index.html << EOF
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="author" content="XGQT"/>
        <meta name="generator" content="GitLab Pages"/>
        <title>Distfiles</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../assets/styles/main.css"/>
    </head>
    <body>
        <h1>
            Gentoo Mini Mirror - Distfiles
        </h1>
EOF

for dist in "${base}"/public/distfiles/*
do
    echo "${dist}"
    name="$(basename "${dist}")"
    cat >> "${base}"/public/distfiles/index.html << EOF
        <a href="${name}">
            ${name}
        </a>
        <br />
EOF
done

cat >> "${base}"/public/distfiles/index.html << EOF
        <br />
        <br />
        <a href="https://gitlab.com/xgqt/gentoo-mini-mirror/-/pipelines">
            <i>Generated on: $(date +"%Y-%m-%d")</i>
        </a>
        <br />
    </body>
</html>
EOF



# Portage

[ -f "${base}"/public/portage/index.html ] && rm "${base}"/public/portage/index.html

cat >> "${base}"/public/portage/index.html << EOF
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="author" content="XGQT"/>
        <meta name="generator" content="GitLab Pages"/>
        <title>Portage</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../assets/styles/main.css"/>
    </head>
    <body>
        <h1>
            Gentoo Mini Mirror - Portage
        </h1>
EOF

for cod in "${base}"/public/portage/*
do
    if [ -d "${cod}" ]
    then
        cat >> "${base}"/public/portage/index.html << EOF
        <b>$(basename "${cod}")</b>
        <br />
EOF
        for cod_d in "${cod}"/*
        do
            echo "${cod_d}"
            name="$(basename "${cod}")/$(basename "${cod_d}")"
            cat >> "${base}"/public/portage/index.html << EOF
            <a href="${name}">
                $(basename "${cod_d}")
            </a>
            <br />
EOF
        done
    elif [ -f "${cod}" ]
    then
        echo "${cod}"
        name="./$(basename "${cod}")"
        cat >> "${base}"/public/portage/index.html << EOF
            <a href="${name}">
                <b>${name}</b>
            </a>
            <br />
EOF
    fi
done

cat >> "${base}"/public/portage/index.html << EOF
        <br />
        <br />
        <a href="https://gitlab.com/xgqt/gentoo-mini-mirror/-/pipelines">
            <i>Generated on: $(date +"%Y-%m-%d")</i>
        </a>
        <br />
    </body>
</html>
EOF
