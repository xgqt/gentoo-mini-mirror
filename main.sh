#!/usr/bin/env bash


# This file is part of gentoo-mini-mirror.

# gentoo-mini-mirror is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# gentoo-mini-mirror is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with gentoo-mini-mirror.  If not, see <https://www.gnu.org/licenses/>.

# shellcheck disable=2016,2129


trap 'exit 128' INT
export PATH
set -e


required_dirs=(
    ./public/binpkgs
    ./public/distfiles
    ./public/portage
    /etc/portage/package.accept_keywords
    /etc/portage/package.use
    /etc/portage/repos.conf
    /var/cache/binpkgs
    /var/cache/distfiles
    /var/db/repos/gentoo
)


for rd in "${required_dirs[@]}"
do
    mkdir -pv "${rd}"
done

emerge-webrsync

# We don't need to read this in CI logs
eselect news read new >/dev/null 2>&1

export MAKEOPTS="--jobs=10"
export EMERGE_DEFAULT_OPTS="--jobs=10 --quiet --quiet-build=y"
export PYTHON_TARGETS="python3_8"
export USE="${USE} -perl"

emerge -ef @system >/dev/null

emerge -1Ou dev-python/certifi dev-python/setuptools
emerge -1n app-portage/cpuid2cpuflags app-portage/portage-utils dev-vcs/git

eselect profile set --force default/linux/amd64/17.1
bash ./install-overlay.sh https://gitlab.com/xgqt/myov.git

echo 'app-admin/pystow' >> /etc/portage/package.accept_keywords/zz-pystow
echo 'app-portage/genlica **' >> /etc/portage/package.accept_keywords/zz-genlica

emerge -1n app-admin/pystow app-portage/genlica
bash /opt/genlica/install

echo 'COMMON_FLAGS="-mtune=generic -O3 -falign-functions=32 -pipe"' > /etc/portage/make.conf/02-flags.conf
echo 'CPU_FLAGS_X86="mmx mmxext sse sse2"' > /etc/portage/make.conf/00-x86.conf
echo 'EMERGE_DEFAULT_OPTS="${EMERGE_DEFAULT_OPTS} --jobs=10"' > /etc/portage/make.conf/03-emerge.conf
echo 'MAKEOPTS="--jobs=10"' > /etc/portage/make.conf/01-makeopts.conf
echo '*/* -X -cups -gtk -gtk3 -v4l -xft -xinerama' >> /etc/portage/package.use/zz-nogui.conf
echo '*/* -alsa -networkmanager -pulseaudio -sound' >> /etc/portage/package.use/zz-nogui.conf
echo '*/* -gif -jpeg -png -raw -svg -xpm' >> /etc/portage/package.use/zz-nogui
echo '*/* bindist' > /etc/portage/package.use/zz-bindist.conf
echo 'app-crypt/pinentry -gnome-keyring' >> /etc/portage/package.use/zz-nogui
echo 'media-libs/imlib2 -shm' >> /etc/portage/package.use/zz-nogui.conf
echo 'www-client/w3m -imlib' >> /etc/portage/package.use/zz-nogui.conf

emerge -1n @cs-portage

quickpkg --include-config=y --umask=0000  "*/*"

cp -Lrv /etc/portage/* ./public/portage/
cp -Lrv /var/cache/binpkgs/* ./public/binpkgs/
cp -Lrv /var/cache/distfiles/* ./public/distfiles/
rm -dr ./public/binpkgs/virtual

bash ./htmlize.sh
emerge --info | tee -a ./public/portage/info.txt

du -sh ./public | tee -a ./public/portage/size.txt
