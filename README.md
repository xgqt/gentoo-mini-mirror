# Gentoo Mini Mirror

<p align="center">
    <a href="https://gitlab.com/xgqt/gentoo-mini-mirror/pipelines">
        <img src="https://gitlab.com/xgqt/gentoo-mini-mirror/badges/master/pipeline.svg">
    </a>
    <a href="https://gitlab.com/xgqt/gentoo-mini-mirror/commits/master.atom">
        <img src="https://img.shields.io/badge/feed-atom-orange.svg">
    </a>
    <a href="./LICENSE">
        <img src="https://img.shields.io/badge/license-GPLv3-blue.svg">
    </a>
</p>

This project is unaffiliated with the Gentoo Project or the Gentoo Foundation.

This is a experiment on how to generate a theoretical Linux distribution mirror using Gitlab Pages.
